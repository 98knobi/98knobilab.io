![Build Status](https://gitlab.com/98knobi/98knobi.gitlab.io/badges/master/build.svg)

<!-- TOC -->

- [Sources](#sources)

<!-- /TOC -->

# Sources

For syntax highlighting i use the syntax.css Stylesheet by the Github-user [mojombo](https://github.com/mojombo) found in [this repository](https://github.com/mojombo/tpw).
Modded it to a dark theme. 